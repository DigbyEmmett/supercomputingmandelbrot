#include "header.h"
#include <mpi.h>
#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_num_threads() 1
    #define omp_get_thread_num() 0
#endif
int main(int argc, char* argv[]) {
  int size=0,rank=0;

  SP x=-1,y=0,sx=1,sy=1,z=1;

  int w=1336,h=780;
  int lastnpixels;
  int pixels,nslave;
  int npixels;

  // now parse all the data which is sent through command line arguments
  if(argc>=3)
  {
    sscanf(argv[1],"%d",&w);
    sscanf(argv[2],"%d",&h);
    sx = ((double)w/h);
  }
  if(argc>=5)
  {
    sscanf(argv[3],"%lf",&x);
    sscanf(argv[4],"%lf",&y);
  }
  if(argc>=6)
  {
    sscanf(argv[5],"%lf",&z);
    sx *= z;
    sy *= z;
  }
  //now subdivide tasks into even ranges
  pixels = w*h;
  MPI_Init(&argc,&argv); 
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);


  if(size!=1)
  {
    /*first figure out how many slaves we have * ¯\_(ツ)_/¯ */
    nslave = size-1;                          /*      |     */
                                              /*    _/ \_   */
    //essentially divide teh number of pixels by the number of workers as a float
    //then round the number down to get the actual pixels
    //finally find the coutn for the last node which may be slightly
    // more thant the others
    
    npixels = pixels/nslave; 
    lastnpixels = pixels - npixels*(nslave-1);
      // I AM THE MASTER THREAD SO I TELL EVERYONE WHAT TO DO
    if(rank==0)
    { 
      //FINALLY WE are only going to wait to recieve the results of the nodes work
      printf("Generating Image: %dx%d at %f %f, sx: %f sy:%f\n",
          w,h,x,y,sx,sy);
      struct dataStruct* allbytes = malloc(sizeof(struct dataStruct)*pixels);
      MPI_Request request[nslave];
      MPI_Status status[nslave];
      for(int i=0;i<nslave;i++)
      {
        int fragmentS = (i!=nslave?npixels:lastnpixels);
      
        int a = MPI_Irecv(allbytes+i*npixels,fragmentS,MPI_UNSIGNED_LONG_LONG,i+1,0,
          MPI_COMM_WORLD,request + i);
        printf("%d %d %d %d\n",i,nslave,fragmentS,a);
      fflush(stdout);
      }
      printf("%d\n",MPI_Waitall(nslave,request,status));
      //lolollll now we write to the file because were done
      char * output = getOutputFilename();
      FILE* f = fopen(output,"wb");
      fwrite(&w,sizeof(int),1,f);
      fwrite(&h,sizeof(int),1,f);
      for(int i=0;i<pixels;i+=w)
      {
        fwrite(allbytes+i,sizeof(struct dataStruct),w,f);
      } 
      fclose(f);
      free(allbytes);
    }
    else//I AM A SLAVE TO THE MASTER AWAIT INSTRUCTIONS
    {
      //HURRY UP AND DO YOUR JOB STOP READING THIS
      int offset;
      MPI_Request sendRequest;
      int bytelength = (rank!=nslave+1?npixels:lastnpixels);
      printf("&d\n",bytelength);
      offset = (rank-1)*npixels;
      struct dataStruct* bytes;
      bytes = malloc(sizeof(struct dataStruct)*bytelength);
      #pragma omp parallel shared(bytes)
      {
        int threadnum = omp_get_thread_num();
        int numthreads = omp_get_num_threads();
        for(int i=threadnum;i<bytelength;i+=numthreads)
        {
          SP a,b;
          int ix,iy,pos = i+offset;
          ix = pos%w;
          iy = (pos-ix)/w;
          a = x-sx/2+(sx/w)*ix;
          b = y+sy/2-(sy/h)*iy;
          //after exhausting all 10000000 permutations this one works
          iterate(a,b,&bytes[i].I);
        }
      }
      // once we are done with this we can send the bytes back to the master as an
      //array
      MPI_Send(bytes,bytelength,MPI_UNSIGNED_LONG_LONG,0,0,
        MPI_COMM_WORLD); 
      free(bytes);
    }
  }
  else
  {
    printf("Nothing to do. not enough tasks for multiprocessing\n");
  }
  MPI_Finalize();
}
