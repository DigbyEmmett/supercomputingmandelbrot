# SupercomputingMandelbrot 
   
The purpose of this project is to produce detailed mandelbrot images on a slurm supercomputing system. 
This project has branches which utilise both the OpenMP and MPI functionality which is available at Pawsey. 
 
##Use 
 
To configure executables for your system first navigate to the appropriate branch for the executable which you are looking for. 
The branches may be any of the following: 
 
 - master: the serial,omp,mpi versions of the code used for bench marking the other pieces of code. This also contains the code for the openMP implementation. 
 - mpfr: mpfr version of the code which is to be run. This version has arbitrary precision and is much slower than the other also the output is basic 
 
Simply run git checkout <branch-name> to retrieve the code for the branch then run 
 
```bash 
make <mandels|mandelomp|mandelmpi> -B 
#you should include -B to ensure mandel.c is recompiled (for mpi) 
``` 
An executable will also be produced. this can be run with 
 
```bash 
sbatch run.slurm <s|omp|mpi> <w> <h> <x> <y> <y-height> 
#y-height is the total height range for the image, Width is scaled proportionally to this height. y-height is normally small 
``` 
 
An output file will apear with the name "outputs/slurm-XXXXXX.out" where XXXXXX is the job ID.  
 
I suggest you use 
```bash 
cat outputs/slurm-XXXXXX.out 
``` 
to find the name of the output data file then run 
```bash 
make imagify 
./imagify XXXXXXXXXXX.d 
``` 
to generate an image from the output data. view this image with 
```bash 
gnome-open output.png 
``` 

# MPI DOES WORK BUT ITS A BIT WEIRD

## Instructions

You will need to modify the run.slurm file to use 4 nodes not 1 node.

  - simply open run.slurm and change the line
```bash
#SBATCH --nodes=1 --> #SBATCH --nodes=4
```
you can choose how many nodes you want but make sure you change both the `#SBATCH` and `srun` lines to match each other

:smile: :smile: :smile: :thumbsup: :thumbsup:

The time allocation should be plenty for images which are below 16K resolution. (In theroy this shouldnt take longer than 5 mintues I think).
If you want to modify it you can but note that serial tasks perform much worse than OMP and MPI because it is inefficient
