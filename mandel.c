#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_num_threads() 1
    #define omp_get_thread_num() 0
#endif

#define MAX_ITER 3000

#include <stdlib.h>
#include "header.h"
#include <stdio.h>
#include <string.h>



void iterate(SP x, SP y, unsigned long long* I) {
    struct {
        SP X;
        SP Y;
    } Z;
    SP H = 0;
    Z.X = x;
    Z.Y = y;
    unsigned long long iter;
    SP tmp;
    iter = 0;
    while(H<4&&iter<MAX_ITER) {
        //expandec form of the Z^2 +C formula for real and imaginary
        tmp = Z.X;
        Z.X = pow(Z.X,2)-pow(Z.Y,2)+x;
        Z.Y = 2*tmp*Z.Y+y;
        H = pow(Z.X,2)+pow(Z.Y,2);
        iter++;
    }
    *I = iter;
}
#ifndef USE_MPI
void gen(SP a,SP b, SP sa, SP sb,int w, int h)  {
    long long res;
    long long avg;
    SP z;
    long long maxres;
    char* output;
    FILE* f;
    output = getOutputFilename();
    f = fopen(output,"wb");
    fwrite(&w,sizeof(int),1,f);
    fwrite(&h,sizeof(int),1,f);
    printf("Generating Image: %dx%d at %.30lf %.30lf, sx: %.30lf sy:%.30lf",
          w,h,a,b,sa,sb);
    struct dataStruct* bytes = malloc(sizeof(struct dataStruct)*w*h); 
    //iterate over all values of height and width.
    //This is done like this so that it is easier to make parallel
    #pragma omp parallel shared(bytes)
    {
      int i=0;
      int start = omp_get_thread_num();
      for(i=start;i<w*h;i+=omp_get_num_threads())
      {
        int x,y;
        SP xc,yc;
        x = i%w;
        y = (i-x)/w;
        xc = a-sa/2+(sa/w)*x;
        yc = b-sb/2+(sb/h)*y;
        iterate(xc,yc,&bytes[i].I);
      }
    }
    int i;
    for(i=0;i<h;i++)
    {
      fwrite(bytes+i*w,sizeof(struct dataStruct),w,f);
    }
    
    fclose(f);
    return;
}

int main(int argc, char* argv[]) {
      SP x,y,sx,sy,z;
      int w,h;
      char *end;
      x = -1;
      y = 0;
      sx = 1;
      sy = 1;
      z = 1;
      w = 1336;
      h = 780;
  if(argc>=3)
  {
    sscanf(argv[1],"%d",&w);
    sscanf(argv[2],"%d",&h);
    sx = ((double)w/h);
  }
  if(argc>=5)
  {
    sscanf(argv[3],"%lf",&x);
    sscanf(argv[4],"%lf",&y);
  }
  if(argc>=6)
  {
    sscanf(argv[5],"%lf",&z);
    sx *= z;
    sy *= z;
  }
    else {
      printf("Syntax:\n<executable> width height x y zoom...\n");
      printf("But im still gonna do what I can\n");
    }
    gen(x,y,sx,sy,w,h);
}
#endif
