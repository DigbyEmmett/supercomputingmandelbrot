#include "libattopng/libattopng.h"
#include "header.h"
int main(int argc, char* argv[]) {
    FILE* f;
    ld Z;
    ii w,h;
    ll I;
    int x,y;
    double p;
    char output;
    libattopng_t* png;
    if(argc>1)
    {
        f = fopen(argv[1],"rb");
        printf("Opened %s",argv[1]);
    }
    else
    {
        f = fopen("data.d","rb");
    }
    fread(w.bytes,4,1,f);
    fread(h.bytes,4,1,f);
    printf("%d,%d\n",w.n,h.n);
    png = libattopng_new(w.n,h.n,PNG_GRAYSCALE);
    libattopng_start_stream(png,0,0);
    for(x=0;x<h.n;x++) {
        for(y=0;y<w.n;y++) {
            fread(I.bytes,sizeof(long long),1,f);
            //fseek(f,8,SEEK_CUR);
            p = ((I.n%512)-256);
            p=(p<0?-p:p);
           
            libattopng_put_pixel(png,p);
        }
    }
    if(argc>2) {
        libattopng_save(png,argv[2]);
    }
    else
    {
        libattopng_save(png,"output.png");
    }
    libattopng_destroy(png);
    return 0;
}
