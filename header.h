#include <stdio.h>
#include <stdlib.h>
#include <math.h>
typedef long double SP;
struct dataStruct {
  unsigned long long I;
};
typedef struct {
  SP x;
  SP y;
  SP sx;
  SP sy;
} frameInfo;
typedef union {
    char bytes[8];
    long long n;
} ll;
typedef union {
    char bytes[4];
    int n;
} ii;
typedef union {
    char bytes[sizeof(SP)];
    SP n;
} ld;
void iterate(SP,SP,unsigned long long*);
void gen(SP,SP,SP,SP,int,int);
char* getOutputFilename();
