CC=cc
PRGENV=${PE_ENV}
ifeq ($(PRGENV),INTEL)
	OMP_FLAG=-openmp
else ifeq ($(PRGENV),GNU)
	OMP_FLAG=-fopenmp
else ifeq ($(PRGENV),CRAY)
	OMP_FLAG=-homp
endif 

all:	s omp mpi

s:	mandels

omp: mandelomp

mpi: mandelmpi

mandels:	lib.o	mandel.o utils.o
	cc mandel.o lib.o utils.o -o mandels -lm $(CFLAGS)

mandelomp:	lib.o mandel.o utils.o
	cc mandel.o lib.o utils.o -o mandelomp -lm $(CFLAGS) $(OMP_FLAG)

mandelmpi:	lib.o mandelmpi.o utils.o
	cc mandelmpi.o lib.o utils.o mandel.o -o mandelmpi -D -lm $(CFLAGS) $(OMP_FLAG)

imagify: imagify.o lib.o utils.o
	cc imagify.o lib.o utils.o -o imagify -lm $(CFLAGS)

imagify.o:  imagify.c header.h
	cc -c imagify.c

mandel.o:	mandel.c header.h
	cc -c mandel.c

mandelmpi.o:	mandelmpi.c header.h
	cc -c mandel.c -D USE_MPI
	cc -c mandelmpi.c

lib.o:	libattopng/libattopng.c
	cc -c libattopng/libattopng.c -o lib.o

utils.o:	utils.c header.h
	cc -c utils.c

clean:
	rm -f *.o mandle imagify *.png data.d mandel mandelmpi mandelomp
